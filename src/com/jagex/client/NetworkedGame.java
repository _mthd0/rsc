// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) nonlb 

package com.jagex.client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.math.BigInteger;

// Referenced classes of package com.jagex.client:
//			GameShell, a

public class NetworkedGame extends GameShell {
	public void s(BigInteger arg0, BigInteger arg1) {
		pd = arg0;
		qd = arg1;
	}

	public int getSessionID() {
		try {
			String s1 = getParameter("ranseed");
			String s2 = s1.substring(0, 10);
			int clientSesssionID = Integer.parseInt(s2);
			if (clientSesssionID == 0x3ade68b1) {
				byte keys[] = new byte[4];
				com.jagex.Util.readFully("uid.dat", keys, 4);
				clientSesssionID = com.jagex.Util.readInt32(keys, 0);
			}
			return clientSesssionID;
		} catch (Exception _ex) {
			return 0;
		}
	}

	public void login(String user, String pass, boolean reconnecting) {
		if (sd > 0) {
			drawMessage(messageTable[6], messageTable[7]);
			try {
				Thread.sleep(2000L);
			} catch (Exception _ex) {
			}
			drawMessage(messageTable[8], messageTable[9]);
			return;
		}
		try {
			username = user;
			user = com.jagex.Util.formatString(user, 20);
			password = pass;
			pass = com.jagex.Util.formatString(pass, 20);
			if (user.trim().length() == 0) {
				drawMessage(messageTable[0], messageTable[1]);
				return;
			}
			if (reconnecting)
				d(messageTable[2], messageTable[3]);
			else
				drawMessage(messageTable[6], messageTable[7]);
			if (appletStarted())
				stream = new Connection(host, this, port);
			else
				stream = new Connection(host, null, port);
			stream.oe = xc;
			int ssi = stream.readInt32();
			serverSessionID = ssi;
			System.out.println("Session id: " + ssi);
			if (reconnecting)
				stream.beginFrame(19);
			else
				stream.beginFrame(0);
			stream.putInt16(wc);
			stream.putInt64(com.jagex.Util.encode37(user));
			stream.putLineRSA(pass, ssi, pd, qd);
			stream.putInt32(getSessionID());
			stream.flush();
			stream.read();
			int response = stream.read();
			System.out.println("Login response: " + response);
			if (response == 0) {
				ed = 0;
				e_();
				return;
			}
			if (response == 1) {
				ed = 0;
				a();
				return;
			}
			if (reconnecting) {
				user = "";
				pass = "";
				f();
				return;
			}
			if (response == 3) {
				drawMessage(messageTable[10], messageTable[11]);
				return;
			}
			if (response == 4) {
				drawMessage(messageTable[4], messageTable[5]);
				return;
			}
			if (response == 5) {
				drawMessage(messageTable[16], messageTable[17]);
				return;
			}
			if (response == 6) {
				drawMessage(messageTable[18], messageTable[19]);
				return;
			}
			if (response == 7) {
				drawMessage(messageTable[20], messageTable[21]);
				return;
			}
			if (response == 11) {
				drawMessage(messageTable[22], messageTable[23]);
				return;
			}
			if (response == 12) {
				drawMessage(messageTable[24], messageTable[25]);
				return;
			}
			if (response == 13) {
				drawMessage(messageTable[14], messageTable[15]);
				return;
			}
			if (response == 14) {
				drawMessage(messageTable[8], messageTable[9]);
				sd = 1500;
				return;
			}
			if (response == 15) {
				drawMessage(messageTable[26], messageTable[27]);
				return;
			}
			if (response == 16) {
				drawMessage(messageTable[28], messageTable[29]);
				return;
			} else {
				drawMessage(messageTable[12], messageTable[13]);
				return;
			}
		} catch (Exception exception) {
			System.out.println(String.valueOf(exception));
		}
		if (ed > 0) {
			try {
				Thread.sleep(5000L);
			} catch (Exception _ex) {
			}
			ed--;
			login(username, password, reconnecting);
		}
		if (reconnecting) {
			username = "";
			password = "";
			f();
		} else {
			drawMessage(messageTable[12], messageTable[13]);
		}
	}

	public void disconnect() {
		if (stream != null)
			try {
				stream.beginFrame(1);
				stream.flush();
			} catch (IOException _ex) {
			}
		username = "";
		password = "";
		f();
	}

	public void reconnect() {
		System.out.println("Lost connection");
		ed = 10;
		login(username, password, true);
	}

	public void d(String arg0, String arg1) {
		Graphics g1 = getGraphics();
		Font font = new Font("Helvetica", 1, 15);
		int i1 = jj();
		int j1 = qj();
		g1.setColor(Color.black);
		g1.fillRect(i1 / 2 - 140, j1 / 2 - 25, 280, 50);
		g1.setColor(Color.white);
		g1.drawRect(i1 / 2 - 140, j1 / 2 - 25, 280, 50);
		dj(g1, arg0, font, i1 / 2, j1 / 2 - 10);
		dj(g1, arg1, font, i1 / 2, j1 / 2 + 10);
	}

	public void createAccount(String username, String password) {
		if (sd > 0) {
			drawMessage(messageTable[6], messageTable[7]);
			try {
				Thread.sleep(2000L);
			} catch (Exception _ex) {
			}
			drawMessage(messageTable[8], messageTable[9]);
			return;
		}
		try {
			username = com.jagex.Util.formatString(username, 20);
			password = com.jagex.Util.formatString(password, 20);
			drawMessage(messageTable[6], messageTable[7]);
			if (appletStarted())
				stream = new Connection(host, this, port);
			else
				stream = new Connection(host, null, port);
			int ssi = stream.readInt32();
			serverSessionID = ssi;
			System.out.println("Session id: " + ssi);
			int referrerID = 0;
			try {
				if (appletStarted())
					referrerID = Integer.parseInt(getParameter("referid"));
			} catch (Exception _ex) {
			}
			stream.beginFrame(2);
			stream.putInt16(wc);
			stream.putInt64(com.jagex.Util.encode37(username));
			stream.putInt16(referrerID);
			stream.putLineRSA(password, ssi, pd, qd);
			stream.putInt32(getSessionID());
			stream.flush();
			stream.read();
			int response = stream.read();
			stream.close();
			System.out.println("Newplayer response: " + response);
			if (response == 2) {
				u();
				return;
			}
			if (response == 3) {
				drawMessage(messageTable[14], messageTable[15]);
				return;
			}
			if (response == 4) {
				drawMessage(messageTable[4], messageTable[5]);
				return;
			}
			if (response == 5) {
				drawMessage(messageTable[16], messageTable[17]);
				return;
			}
			if (response == 6) {
				drawMessage(messageTable[18], messageTable[19]);
				return;
			}
			if (response == 7) {
				drawMessage(messageTable[20], messageTable[21]);
				return;
			}
			if (response == 11) {
				drawMessage(messageTable[22], messageTable[23]);
				return;
			}
			if (response == 12) {
				drawMessage(messageTable[24], messageTable[25]);
				return;
			}
			if (response == 13) {
				drawMessage(messageTable[14], messageTable[15]);
				return;
			}
			if (response == 14) {
				drawMessage(messageTable[8], messageTable[9]);
				sd = 1500;
				return;
			}
			if (response == 15) {
				drawMessage(messageTable[26], messageTable[27]);
				return;
			}
			if (response == 16) {
				drawMessage(messageTable[28], messageTable[29]);
				return;
			} else {
				drawMessage(messageTable[12], messageTable[13]);
				return;
			}
		} catch (Exception exception) {
			System.out.println(String.valueOf(exception));
		}
		drawMessage(messageTable[12], messageTable[13]);
	}

	public void j() {
		long l1 = System.currentTimeMillis();
		if (stream.ob())
			fd = l1;
		if (l1 - fd > 5000L) {
			fd = l1;
			stream.beginFrame(5);
			stream.endFrame();
		}
		try {
			stream.writeFrame(20);
		} catch (IOException _ex) {
			reconnect();
			return;
		}
		if (!w())
			return;
		int i1 = stream.pb(dd);
		if (i1 > 0)
			c(dd[0] & 0xff, i1);
	}

	public void c(int arg0, int arg1) {
		if (arg0 == 8) {
			String s1 = new String(dd, 1, arg1 - 1);
			g(s1);
		}
		if (arg0 == 9)
			disconnect();
		if (arg0 == 10) {
			k();
			return;
		}
		if (arg0 == 23) {
			gd = com.jagex.Util.unsign(dd[1]);
			for (int i1 = 0; i1 < gd; i1++) {
				hd[i1] = com.jagex.Util.on(dd, 2 + i1 * 9);
				id[i1] = com.jagex.Util.unsign(dd[10 + i1 * 9]);
			}

			z();
			return;
		}
		if (arg0 == 24) {
			long l1 = com.jagex.Util.on(dd, 1);
			int k1 = dd[9] & 0xff;
			for (int i2 = 0; i2 < gd; i2++)
				if (hd[i2] == l1) {
					if (id[i2] == 0 && k1 != 0)
						g("@pri@" + com.jagex.Util.decode37(l1) + " has logged in");
					if (id[i2] != 0 && k1 == 0)
						g("@pri@" + com.jagex.Util.decode37(l1) + " has logged out");
					id[i2] = k1;
					arg1 = 0;
					z();
					return;
				}

			hd[gd] = l1;
			id[gd] = k1;
			gd++;
			g("@pri@" + com.jagex.Util.decode37(l1)
					+ " has been added to your friends list");
			z();
			return;
		}
		if (arg0 == 26) {
			jd = com.jagex.Util.unsign(dd[1]);
			for (int j1 = 0; j1 < jd; j1++)
				kd[j1] = com.jagex.Util.on(dd, 2 + j1 * 8);

			return;
		}
		if (arg0 == 27) {
			ld = dd[1];
			md = dd[2];
			nd = dd[3];
			od = dd[4];
			return;
		}
		if (arg0 == 28) {
			long l2 = com.jagex.Util.on(dd, 1);
			String s2 = com.jagex.Util.nn(dd, 9, arg1 - 9, true);
			g("@pri@" + com.jagex.Util.decode37(l2) + ": tells you " + s2);
			return;
		} else {
			n(arg0, arg1, dd);
			return;
		}
	}

	public void z() {
		boolean flag = true;
		while (flag) {
			flag = false;
			for (int i1 = 0; i1 < gd - 1; i1++)
				if (id[i1] < id[i1 + 1]) {
					int j1 = id[i1];
					id[i1] = id[i1 + 1];
					id[i1 + 1] = j1;
					long l1 = hd[i1];
					hd[i1] = hd[i1 + 1];
					hd[i1 + 1] = l1;
					flag = true;
				}

		}
	}

	public void x(String arg0, String arg1) {
		arg0 = com.jagex.Util.formatString(arg0, 20);
		arg1 = com.jagex.Util.formatString(arg1, 20);
		stream.beginFrame(25);
		stream.putLineRSA(arg0 + arg1, serverSessionID, pd, qd);
		stream.endFrame();
	}

	public void h(int arg0, int arg1, int arg2, int arg3) {
		stream.beginFrame(31);
		stream.putInt8(arg0);
		stream.putInt8(arg1);
		stream.putInt8(arg2);
		stream.putInt8(arg3);
		stream.endFrame();
	}

	public void ignoreUser(String username) {
		long l1 = com.jagex.Util.encode37(username);
		stream.beginFrame(29);
		stream.putInt64(l1);
		stream.endFrame();
		for (int i1 = 0; i1 < jd; i1++)
			if (kd[i1] == l1)
				return;

		if (jd >= 50) {
			return;
		} else {
			kd[jd++] = l1;
			return;
		}
	}

	public void o(long arg0) {
		stream.beginFrame(30);
		stream.putInt64(arg0);
		stream.endFrame();
		for (int i1 = 0; i1 < jd; i1++)
			if (kd[i1] == arg0) {
				jd--;
				for (int j1 = i1; j1 < jd; j1++)
					kd[j1] = kd[j1 + 1];

				return;
			}
	}

	public void addFriend(String username) {
		stream.beginFrame(26);
		stream.putInt64(com.jagex.Util.encode37(username));
		stream.endFrame();
	}

	public void removeFriend(long user) {
		stream.beginFrame(27);
		stream.putInt64(user);
		stream.endFrame();
		for (int i1 = 0; i1 < gd; i1++) {
			if (hd[i1] != user)
				continue;
			gd--;
			for (int j1 = i1; j1 < gd; j1++) {
				hd[j1] = hd[j1 + 1];
				id[j1] = id[j1 + 1];
			}

			break;
		}

		g("@pri@" + com.jagex.Util.decode37(user)
				+ " has been removed from your friends list");
	}

	public void sendPrivateMessage(long username, byte messageBytes[], int arg2) {
		stream.beginFrame(28);
		stream.putInt64(username);
		stream.readBytes(messageBytes, 0, arg2);
		stream.endFrame();
	}

	public void q(byte arg0[], int arg1) {
		stream.beginFrame(3);
		stream.readBytes(arg0, 0, arg1);
		stream.endFrame();
	}

	public void sendCommand(String command) {
		stream.beginFrame(7);
		stream.putLine(command);
		stream.endFrame();
	}

	public void drawMessage(String s1, String s2) {
	}

	public void a() {
	}

	public void e_() {
	}

	public void f() {
	}

	public void k() {
	}

	public void u() {
	}

	public void n(int i1, int j1, byte abyte0[]) {
	}

	public void g(String s1) {
	}

	public boolean w() {
		return true;
	}

	public NetworkedGame() {
		host = "127.0.0.1";
		port = 43594;
		username = "";
		password = "";
		dd = new byte[5000];
		hd = new long[100];
		id = new int[100];
		kd = new long[50];
	}

	public static String messageTable[];
	public static int wc = 1;
	public static int xc;
	public String host;
	public int port;
	String username;
	String password;
	public Connection stream;
	byte dd[];
	int ed;
	long fd;
	public int gd;
	public long hd[];
	public int id[];
	public int jd;
	public long kd[];
	public int ld;
	public int md;
	public int nd;
	public int od;
	public BigInteger pd;
	public BigInteger qd;
	public int serverSessionID;
	public int sd;

	static {
		messageTable = new String[50];
		messageTable[0] = "You must enter both a username";
		messageTable[1] = "and a password - Please try again";
		messageTable[2] = "Connection lost! Please wait...";
		messageTable[3] = "Attempting to re-establish";
		messageTable[4] = "That username is already in use.";
		messageTable[5] = "Wait 60 seconds then retry";
		messageTable[6] = "Please wait...";
		messageTable[7] = "Connecting to server";
		messageTable[8] = "Sorry! The server is currently full.";
		messageTable[9] = "Please try again later";
		messageTable[10] = "Invalid username or password.";
		messageTable[11] = "Try again, or create a new account";
		messageTable[12] = "Sorry! Unable to connect to server.";
		messageTable[13] = "Check your internet settings";
		messageTable[14] = "Username already taken.";
		messageTable[15] = "Please choose another username";
		messageTable[16] = "The client has been updated.";
		messageTable[17] = "Please reload this page";
		messageTable[18] = "You may only use 1 character at once.";
		messageTable[19] = "Your ip-address is already in use";
		messageTable[20] = "Login attempts exceeded!";
		messageTable[21] = "Please try again in 5 minutes";
		messageTable[22] = "Account has been temporarily disabled";
		messageTable[23] = "for cheating or abuse";
		messageTable[24] = "Account has been permanently disabled";
		messageTable[25] = "for cheating or abuse";
		messageTable[26] = "You need a members account";
		messageTable[27] = "to login to this server";
		messageTable[28] = "Please login to a members server";
		messageTable[29] = "to access member-only features";
	}
}