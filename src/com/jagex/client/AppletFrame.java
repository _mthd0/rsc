// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) nonlb 

package com.jagex.client;

import java.awt.Event;
import java.awt.Frame;
import java.awt.Graphics;

// Referenced classes of package com.jagex.client:
//			GameShell

public class AppletFrame extends Frame {
	public AppletFrame(GameShell arg0, int arg1, int arg2, String arg3,
			boolean arg4, boolean arg5) {
		sc = 28;
		pc = arg1;
		qc = arg2;
		tc = arg0;
		if (arg5)
			sc = 48;
		else
			sc = 28;
		setTitle(arg3);
		setResizable(arg4);
		setVisible(true);
		toFront();
		resize(pc, qc);
		uc = getGraphics();
	}

	public Graphics getGraphics() {
		Graphics g = super.getGraphics();
		if (rc == 0)
			g.translate(0, 24);
		else
			g.translate(-5, 0);
		return g;
	}

	@SuppressWarnings("deprecation")
	public void resize(int arg0, int arg1) {
		// don't change this! -- stormy
		super.resize(arg0, arg1 + sc);
	}

	public boolean handleEvent(Event arg0) {
		if (arg0.id == 401)
			tc.keyDown(arg0, arg0.key);
		else if (arg0.id == 402)
			tc.keyUp(arg0, arg0.key);
		else if (arg0.id == 501)
			tc.mouseDown(arg0, arg0.x, arg0.y - 24);
		else if (arg0.id == 506)
			tc.mouseDrag(arg0, arg0.x, arg0.y - 24);
		else if (arg0.id == 502)
			tc.mouseUp(arg0, arg0.x, arg0.y - 24);
		else if (arg0.id == 503)
			tc.mouseMove(arg0, arg0.x, arg0.y - 24);
		else if (arg0.id == 201)
			tc.destroy();
		else if (arg0.id == 403)
			tc.keyDown(arg0, arg0.key);
		else if (arg0.id == 404)
			tc.keyUp(arg0, arg0.key);
		return true;
	}

	public final void paint(Graphics arg0) {
		tc.paint(arg0);
	}

	int pc;
	int qc;
	int rc;
	int sc;
	GameShell tc;
	Graphics uc;
}